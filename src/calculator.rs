
pub use crate::defs::{ADD, SUBTRACT, MULTIPLY, DIVIDE, EMPTY};

pub struct Operation {
    pub value: char,
    pub previous: char,
}

pub struct Calculator {
    pub num1: i32,
    pub num2: i32,
    pub ops: Operation,
    pub num_counter: i16,
}

impl Calculator {
    pub fn set_nums(&mut self, value: i32) {
        /* Set the values for the first or second numbers */
        if self.num_counter == 0 {
            self.num1 = self.num1 * 10 + value;
        }
        else if self.num_counter == 1 {
            self.num2 = self.num2 * 10 + value;
        }
    }
    pub fn set_ops(&mut self, ops: char) {
        /* Set the operator type */
        self.num_counter +=1;

        if self.num_counter == 2 {
            self.ops.previous = self.ops.value;
            self.ops.value = ops;

            match self.ops.previous {
                ADD => self.num1 = self.num1 + self.num2,
                SUBTRACT => self.num1 = self.num1 - self.num2,
                MULTIPLY => self.num1 = self.num1 * self.num2,
                DIVIDE => self.num1 = self.num1 / self.num2,
                _=> ()
            }
            // decrease num counter and reset num2
            self.num_counter -= 1;
            self.num2 = 0;
        }
        else {
            self.ops.value = ops;
            //println!("counter:{}", self.num_counter);
        }
    }
    /*pub fn change(&mut self) {

    }*/
    pub fn result(&mut self) -> i32 {
        let mut result: i32 = 0;
        self.num_counter +=1;

        if self.num_counter == 2 {
            match self.ops.value {
                ADD => {self.num1 = self.num1 + self.num2; result = self.num1;},
                SUBTRACT => {self.num1 = self.num1 - self.num2; result = self.num1;},
                MULTIPLY => {self.num1 = self.num1 * self.num2; result = self.num1;},
                _=> ()
            }
            if self.ops.value == DIVIDE && self.num2 == 0 {
                println!("Divide by zero error");
            }
            else if self.ops.value == DIVIDE && self.num2 != 0 {
                self.num1 = self.num1 / self.num2;
                result = self.num1;
            }

            // reset num counter and reset num1 and num2
            self.num_counter = 0;
            self.num1 = 0;
            self.num2 = 0;
            self.ops.value = EMPTY;
        }
        return result;
    }

}
