/*
 * gtk4 library rust keybindings
 *
 * Documentaiton here:
 * https://gtk-rs.org/gtk4-rs/git/docs/gtk4/index.html
 */ 

use gtk4 as gtk;
use gtk::prelude::*;
use gtk::{Application, ApplicationWindow, Grid, Entry};

mod calculator;
mod button;
mod defs;

pub use crate::calculator::{Operation, Calculator};
pub use crate::defs::{ADD, SUBTRACT, MULTIPLY, DIVIDE, EMPTY};
pub use crate::button::{create_button, connect_num_button};

fn main() {
    // Create a new application
    let app = Application::builder()
        .application_id("com.example.Calculator")
        .build();

    // Connect to "activate" signal of `app`
    app.connect_activate(build_ui);

    // Run the application
    app.run();
}


fn build_ui(application: &Application) {

    let mut number: i32 = 0;

    let mut operation = Operation {value: EMPTY, previous: EMPTY};

    let mut calculator = Calculator {
        num1: 0,
        num2: 0,
        ops: operation,
        num_counter: 0,
    };

    // Test
    /*
    calculator.set_nums(1);
    calculator.set_ops(DIVIDE);
    calculator.set_nums(1);
    let result = calculator.result();
    println!("{}", result);
    */

    let mut window = ApplicationWindow::builder()
        .application(application)
        .title("Calculator")
        .default_width(350)
        .default_height(70)
        .build();

    let mut grid = Grid::new(); // gtk4::Grid
    let mut entry = Entry::new(); // gtk4::Entry

    /* gtk4::Button */
    let mut num1 = create_button("1");
    let mut num2 = create_button("2");
    let mut num3 = create_button("3");
    let mut num4 = create_button("4");
    let mut num5 = create_button("5");
    let mut add = create_button("+");
    let mut equals = create_button("=");

    num1.connect_clicked(move |num1| {
        eprintln!("{}", 1); // std::eprintln
    });

    //connect_num_button(&num1, 1);
    connect_num_button(&num2, 2);
    connect_num_button(&num3, 3);
    connect_num_button(&num4, 4);
    connect_num_button(&num5, 5);

    // gtk4::prelude::GridExt
    GridExt::attach(&grid, &entry, 1, 1, 5, 1);
    GridExt::attach(&grid, &num1, 1, 2, 1, 1);
    GridExt::attach(&grid, &num2, 2, 2, 1, 1);
    GridExt::attach(&grid, &num3, 3, 2, 1, 1);
    GridExt::attach(&grid, &num4, 4, 2, 1, 1);
    GridExt::attach(&grid, &num5, 5, 2, 1, 1);
    GridExt::attach(&grid, &add, 6, 2, 1, 1);
    GridExt::attach(&grid, &equals, 7, 2, 1, 1);

    // gtk4::prelude::GtkWindowExt
    // std::option
    window.set_child(Some(&grid));

    // Present the window
    window.show();
}
