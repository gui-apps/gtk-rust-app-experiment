
/* Operation constants for it to be used */
pub const ADD: char = 'a';
pub const SUBTRACT: char = 's';
pub const MULTIPLY: char = 'm';
pub const DIVIDE: char = 'd';
pub const EMPTY: char = 'e';
