
use gtk4 as gtk;
use gtk::prelude::*;
use gtk::{Button};
pub use crate::calculator::{Operation, Calculator};

pub fn create_button(label: &'static str) -> Button {
    let margin = 2;
    let button = Button::builder()
        .label(label)
        .margin_start(margin)
        .margin_top(margin)
        .margin_end(margin)
        .margin_bottom(margin)
        .build();

    return button;
}


pub fn connect_num_button(button: &Button, value: i32) -> i32 {
    /* gtk4::prelude::ButtonExt */
    /*  Keyword move:
        - Capture a closure’s environment by value.
        - "move" converts any variables captured by
          reference or mutable reference to variables
          captured by value.

    */
    let mut num: i32 = 0;
    button.connect_clicked(move |_| {
        //calculator.set_nums(value);
        eprintln!("{}", value); // std::eprintln
    });
    return num;
}
